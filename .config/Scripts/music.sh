#!/bin/bash
if test ! -e ~/.music
then
	echo "hello" > ~/.music
	mocp -G
	exec kill -STOP $(pidof mpv);
else
	kill -CONT $(pidof mpv);
	mocp -G ;
 	exec rm ~/.music
fi
