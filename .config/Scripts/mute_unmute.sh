#!/bin/bash
volume_mic=$(pamixer --get-volume --source alsa_input.pci-0000_00_1b.0.analog-stereo)
old_volume=$(cat $HOME/.config/Scripts/.volume)
if ! [ $volume_mic -eq 0 ]
then
rm /home/lunaticstraydog/.config/Scripts/.volume
echo $volume_mic > $HOME/.config/Scripts/.volume
pactl -- set-sink-mute alsa_input.pci-0000_00_1b.0.analog-stereo 1
else
pactl -- set-source-volume alsa_input.pci-0000_00_1b.0.analog-stereo $old_volume%
fi
