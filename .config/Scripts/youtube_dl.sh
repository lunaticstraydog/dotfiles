#!/bin/bash
for f in ~/Music/Rock/*;do
	cd  "$f"||exit
	while IFS= read -r line ;do
		youtube-dl -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' -x --audio-quality 0 "$line" -i
	done < urls
	mv urls urls_done

done
for f in ~/Music/Metal/*;do
	cd  "$f"||exit
	while IFS= read -r line ;do
		youtube-dl -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' -x --audio-quality 0 "$line"
	done < urls
	mv urls urls_done

done

