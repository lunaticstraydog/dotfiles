#!/bin/bash
cur_workspace="$(i3-msg -t get_workspaces   | jq '.[] | select(.focused==true).name'   | cut -d"\"" -f2)"
if ! grep -q $cur_workspace "$HOME/.config/Scripts/.work"
then
	echo $cur_workspace >> ~/.config/Scripts/.work
	notify-send "On bosse sévère" -t 3500
	nbr_wspaces=$(wc -l $HOME/.config/Scripts/.work | cut -f 1 -d " " )
	notify-send "$nbr_wspaces espaces de travail en cours d'utilisation" -t 3500
else
	sed -i "/$cur_workspace/d" $HOME/.config/Scripts/.work
	rm ~/.config/Scripts/.Truely_Focused
	notify-send "Bon bah faut bien arreter un moment hein" -t 3500
	nbr_wspaces=$(wc -l $HOME/.config/Scripts/.work | cut -f 1 -d " " )
	notify-send "$nbr_wspaces espaces de travail en cours d'utilisation" -t 3500
fi
