#!/bin/bash
cur_volume="$(pamixer --get-volume)"
old_volume="$(cat ~/.config/Scripts/.old_volume)"
zero=0
if [ "$cur_volume" -eq "$zero" ];
then
	pactl set-sink-volume @DEFAULT_SINK@ "$old_volume"%
else
	pactl set-sink-volume @DEFAULT_SINK@ 0%
	echo $cur_volume > ~/.config/Scripts/.old_volume
fi

