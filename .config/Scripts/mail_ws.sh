#!/bin/bash
if ! pgrep "neomutt"
then
	swaymsg workspace '"11:"; exec termite -e "neomutt -F ~/.config/mutt/muttrc"'
else
	swaymsg workspace "11:"
fi
