" Vimscript file settings ---------------------- {{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType cpp setlocal foldmethod=marker
augroup END
" }}}
" File detection + autocommands---------------------- {{{
augroup filedet
" Runs a script that cleans out tex build files whenever I close out of a .tex file.
autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
map <leader>v :VimwikiIndex
let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
autocmd BufRead,BufNewFile *.tex set filetype=tex
autocmd BufRead,BufNewFile *.py set filetype=python
autocmd BufRead,BufNewFile *.c,*.h set filetype=c
autocmd BufRead,BufNewFile *.c++,*.h++ set filetype=cpp

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Enable Goyo by default for mutt writing
"autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
"autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo
"autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
"autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>
autocmd BufRead,BufNewFile /tmp/neomutt* setlocal spell! spelllang=fr_fr
" Automatically deletes all trailing whitespace and newlines at end of file on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritepre * %s/\n\+\%$//e

" When shortcut files are updated, renew bash and ranger configs with new material:
autocmd BufWritePost files,directories !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
" Update binds when sxhkdrc is updated.
autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
if &diff
	highlight! link DiffText MatchParen
endif
augroup end
" }}}
" Plugins ---------------------- {{{
augroup Plugins
let mapleader =","

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
Plug 'cloudhead/neovim-fuzzy'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'jreybert/vimagit'
Plug 'lukesmithxyz/vimling'
Plug 'vimwiki/vimwiki'
Plug 'bling/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'kovetskiy/sxhkd-vim'
Plug 'ap/vim-css-color'
Plug 'dense-analysis/ale'
call plug#end()
augroup END
" }}}
" Some basics ---------------------- {{{
augroup Plugins
set bg=light
set go=a
set mouse=a
set ignorecase
set smartcase
"set nohlsearch
set clipboard+=unnamedplus
set tabstop=2
"set softtabstop=4
set shiftwidth=2
set noexpandtab
"inoremap <esc> <nop>
"set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber
" Enable autocompletion:
set wildmode=longest,list,full
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o


" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright
" }}}
" nerd tree,vimling, shortcut nav ---------------------- {{{
augroup misc
map <leader>n :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
if has('nvim')
	let NERDTreeBookmarksFile = stdpath('data') . '/NERDTreeBookmarks'
else
	let NERDTreeBookmarksFile = '~/.vim' . '/NERDTreeBookmarks'
endif

nnoremap <C-f> :FuzzyOpen<CR>

" vimling:
nm <leader>d :call ToggleDeadKeys()<CR>
imap <leader>d <esc>:call ToggleDeadKeys()<CR>a
nm <leader>i :call ToggleIPA()<CR>
imap <leader>i <esc>:call ToggleIPA()<CR>
nm <leader>q :call ToggleProse()<CR>

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
"map <C-j> <C-w>j
"map <C-k> <C-w>k
map <C-l> <C-w>l
augroup end
" Some remaps----------------------
augroup remaps
vnoremap g; y/<C-R>"<CR>
vnoremap qq <Esc>`>a'<Esc>`<i'<Esc>
" Navigating with guides
nnoremap <leader><leader> 0<Esc>/<++><Enter>c4l
inoremap <leader><leader> 0<Esc>/<++><Enter>c4l
vnoremap <leader><leader> 0<Esc>/<++><Enter>c4l
map <leader><leader> 0<Esc>/<++><Enter>c4l
"" pour indenter via F7
noremap <F7> gg=G<C-o><C-o>
noremap <space> viw
noremap - ddjp
noremap H 0
noremap zj zb
noremap zk zt
noremap L $
inoremap <c-d> <esc>cc
"inoremap <c-u> <esc>viwUa
"nnoremap <c-u> <esc>viwU
nnoremap dc O<esc>jddk
nnoremap <leader>" viW<esc>a"<esc>Bi"<esc>lel
iabbrev @@    quentin.levent@gmail.com
" Replace ex mode with gq
map Q gq

" Check file in shellcheck:
map <leader>s :!clear && shellcheck %<CR>

" Open my bibliography file in split
map <leader>b :vsp<space>$BIB<CR>
map <leader>r :vsp<space>$REFER<CR>

" Replace all is aliased to S.
nnoremap T :terminal<Enter>
nnoremap S :%s//g<Left><Left>

" Compile document, be it groff/LaTeX/markdown/etc.
"map <leader>c :w! \| !compiler "<c-r>%"<CR>

" Open corresponding .pdf/.html or preview
map <leader>p :!opout <c-r>%<CR><CR>

inoremap jk <esc>
nnoremap <C-j> gT
nnoremap <C-k> gt
nnoremap c "_c
nnoremap ; /
onoremap ; /
" Goyo plugin makes text more readable when writing prose:
map <leader>f :Goyo \| set bg=light \| set linebreak<CR>

" Spell-check set to <leader>o, 'o' for 'orthography':
map <leader>o :setlocal spell! spelllang=fr_fr<CR>
augroup END
" }}}
" LaTex shortcuts---------------------- {{{
augroup LaTeX
"""LATEX
" Word count:
autocmd FileType tex map <leader>w :w !detex \| wc -w<CR>
" Code snippets
autocmd FileType tex inoremap ,fr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter><Enter>\end{frame}<Enter><Enter><++><Esc>6kf}i
autocmd FileType tex inoremap ,fi \begin{fitch}<Enter><Enter>\end{fitch}<Enter><Enter><++><Esc>3kA
autocmd FileType tex inoremap ,exe \begin{exe}<Enter>\ex<Space><Enter>\end{exe}<Enter><Enter><++><Esc>3kA
autocmd FileType tex inoremap ,em \emph{}<++><Esc>T{i
autocmd FileType tex inoremap ,bf \textbf{}<++><Esc>T{i
autocmd FileType tex vnoremap , <ESC>`<i\{<ESC>`>2la}<ESC>?\\{<Enter>a
autocmd FileType tex inoremap ,it \textit{}<++><Esc>T{i
autocmd FileType tex inoremap ,ct \textcite{}<++><Esc>T{i
autocmd FileType tex inoremap ,cp \parencite{}<++><Esc>T{i
autocmd FileType tex inoremap ,glos {\gll<Space><++><Space>\\<Enter><++><Space>\\<Enter>\trans{``<++>''}}<Esc>2k2bcw
autocmd FileType tex inoremap ,x \begin{xlist}<Enter>\ex<Space><Enter>\end{xlist}<Esc>kA<Space>
autocmd FileType tex inoremap ,ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd FileType tex inoremap ,ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd FileType tex inoremap ,li <Enter>\item<Space>
autocmd FileType tex inoremap ,ref \ref{}<Space><++><Esc>T{i
autocmd FileType tex inoremap ,tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
autocmd FileType tex inoremap ,ot \begin{tableau}<Enter>\inp{<++>}<Tab>\const{<++>}<Tab><++><Enter><++><Enter>\end{tableau}<Enter><Enter><++><Esc>5kA{}<Esc>i
autocmd FileType tex inoremap ,can \cand{}<Tab><++><Esc>T{i
autocmd FileType tex inoremap ,con \const{}<Tab><++><Esc>T{i
autocmd FileType tex inoremap ,v \vio{}<Tab><++><Esc>T{i
autocmd FileType tex inoremap ,a \href{}{<++>}<Space><++><Esc>2T{i
autocmd FileType tex inoremap ,sc \textsc{}<Space><++><Esc>T{i
autocmd FileType tex inoremap ,chap \chapter{}<Enter><Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ,sec \section{}<Enter><Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ,ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ,sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
autocmd FileType tex inoremap ,st <Esc>F{i*<Esc>f}i
autocmd FileType tex inoremap ,beg \begin{DELRN}<Enter><++><Enter>\end{DELRN}<Enter><Enter><++><Esc>4k0fR:MultipleCursorsFind<Space>DELRN<Enter>c
autocmd FileType tex inoremap ,up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
autocmd FileType tex nnoremap ,up /usepackage<Enter>o\usepackage{}<Esc>i
autocmd FileType tex inoremap ,tt \texttt{}<Space><++><Esc>T{i
autocmd FileType tex inoremap ,bt {\blindtext}
autocmd FileType tex inoremap ,nu $\varnothing$
autocmd FileType tex inoremap ,col \begin{columns}[T]<Enter>\begin{column}{.5\textwidth}<Enter><Enter>\end{column}<Enter>\begin{column}{.5\textwidth}<Enter><++><Enter>\end{column}<Enter>\end{columns}<Esc>5kA
autocmd FileType tex inoremap ,rn (\ref{})<++><Esc>F}i
augroup END
" }}}
" HTML shortcuts---------------------- {{{
augroup HTML
autocmd FileType html inoremap ,b <b></b><Space><++><Esc>FbT>i
autocmd FileType html inoremap ,it <em></em><Space><++><Esc>FeT>i
autocmd FileType html inoremap ,1 <h1></h1><Enter><Enter><++><Esc>2kf<i
autocmd FileType html inoremap ,2 <h2></h2><Enter><Enter><++><Esc>2kf<i
autocmd FileType html inoremap ,3 <h3></h3><Enter><Enter><++><Esc>2kf<i
autocmd FileType html inoremap ,p <p></p><Enter><Enter><++><Esc>02kf>a
autocmd FileType html inoremap ,a <a<Space>href=""><++></a><Space><++><Esc>14hi
autocmd FileType html inoremap ,e <a<Space>target="_blank"<Space>href=""><++></a><Space><++><Esc>14hi
autocmd FileType html inoremap ,ul <ul><Enter><li></li><Enter></ul><Enter><Enter><++><Esc>03kf<i
autocmd FileType html inoremap ,li <Esc>o<li></li><Esc>F>a
autocmd FileType html inoremap ,ol <ol><Enter><li></li><Enter></ol><Enter><Enter><++><Esc>03kf<i
autocmd FileType html inoremap ,im <img src="" alt="<++>"><++><esc>Fcf"a
autocmd FileType html inoremap ,td <td></td><++><Esc>Fdcit
autocmd FileType html inoremap ,tr <tr></tr><Enter><++><Esc>kf<i
autocmd FileType html inoremap ,th <th></th><++><Esc>Fhcit
autocmd FileType html inoremap ,tab <table><Enter></table><Esc>O
autocmd FileType html inoremap ,gr <font color="green"></font><Esc>F>a
autocmd FileType html inoremap ,dl <dl><Enter><Enter></dl><enter><enter><++><esc>3kcc
autocmd FileType html inoremap &<space> &amp;<space>
autocmd FileType html inoremap á &aacute;
autocmd FileType html inoremap é &eacute;
autocmd FileType html inoremap í &iacute;
autocmd FileType html inoremap ó &oacute;
autocmd FileType html inoremap ú &uacute;
autocmd FileType html inoremap ä &auml;
autocmd FileType html inoremap ë &euml;
autocmd FileType html inoremap ï &iuml;
autocmd FileType html inoremap ö &ouml;
autocmd FileType html inoremap ü &uuml;
autocmd FileType html inoremap ã &atilde;
autocmd FileType html inoremap ẽ &etilde;
autocmd FileType html inoremap ĩ &itilde;
autocmd FileType html inoremap õ &otilde;
autocmd FileType html inoremap ũ &utilde;
autocmd FileType html inoremap ñ &ntilde;
autocmd FileType html inoremap à &agrave;
autocmd FileType html inoremap è &egrave;
autocmd FileType html inoremap ì &igrave;
autocmd FileType html inoremap ò &ograve;
autocmd FileType html inoremap ù &ugrave;
augroup end
" }}}
" Python shortcuts---------------------- {{{
augroup Python
autocmd FileType python inoremap  ,for for<Space>i<Space>in<Space>range<Space>)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++>;<Enter><Esc>c4l
autocmd FileType python inoremap  ,if if<Space>(<++>)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++><Enter><Esc>c4l
autocmd FileType python inoremap  ,main int<Space>main()<Enter><Space>{<Enter><++><Enter>}<Esc>2k/<++><Enter><Esc>c4l
autocmd FileType python inoremap  ,mat int<Space>mat<++>=[<++>,<++>];<Enter>for(int<Space>i=0;i<<++>;i++)<Space><Enter>{<Enter>for(int<Space>j=0;j<<++>;j++)<Enter><Space>{<Enter>mat<++>[i][j]=(float(rand())/RAND_MAX)*100<Enter>}<Enter>}<Esc>8k/<++><Enter><Esc>c4l
autocmd FileType python inoremap  ,tab int<Space>tab<++>[<++>];<Enter>for(int<Space>i=0;i<<++>;i++)<Enter><Space>{<Enter>tab[i]=(float(rand())/RAND_MAX)*100<Enter>}<Esc>5k/<++><Enter><Esc>c4l
autocmd FileType python inoremap  ,fl float<Space><++><Space>=0.0<Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType python inoremap  ,int int<Space><++><Space>=0<Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType python inoremap  ,inc <Esc>gg17jO#include<<++>.h><Esc>/<++><Enter><Esc>c4l
augroup END
" }}}
" Opale Shortcuts---------------------- {{{
augroup Opale

""s/\$\([^][^]*\)\$/\<sc:textLeaf role="mathtex"\>\1\<sc:textLeaf\>/g
autocmd FileType op inoremap  ,tx s/\$\([^][^]*\)\$/\<sc:textLeaf role="mathtex"\>\1\<sc:textLeaf\>/g
autocmd FileType op inoremap  ,for for<Space>(int<Space>i=0;i<<++>;i++)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++>;<Enter><Esc>c4l
autocmd FileType op inoremap  ,for for<Space>(int<Space>i=0;i<<++>;i++)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++>;<Enter><Esc>c4l
autocmd FileType op inoremap  ,for for<Space>(int<Space>i=0;i<<++>;i++)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++>;<Enter><Esc>c4l
augroup END
" }}}
" C Shortcuts---------------------- {{{
augroup C
autocmd FileType c map <leader>c <Esc>:w!<Enter>:!make clean && make \|\| gcc -g -Wall % -o main <Enter> <Enter>:terminal<Enter>./main

autocmd FileType c inoremap  ,for for<Space>(int<Space>i=0;i<<++>;i++)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++>;<Enter><Esc>c4l
autocmd FileType c inoremap  ,if if<Space>(<++>)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,mai int<Space>main(int argc, char ** argv)<Enter><Space>{<Enter><++><Enter>return 0;<Enter>}<Esc>3k/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,sm int<Space>mat<++>=[N,N];<Enter>for(int<Space>i=0;i<N;i++)<Space><Enter>{<Enter>for(int<Space>j=0;j<N;j++)<Enter><Space>{<Enter>mat<++>[i][j]=rand()%100;<Enter>}<Enter>}<Esc>8k/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,st int<Space>tab<++>[N];<Enter>for(int<Space>i=0;i<N;i++)<Enter><Space>{<Enter>tab[i]=rand()%100;<Enter>}<Esc>5k/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,dt <Esc>Oint<Space>*Tab;<Enter>Tab=<Space>(int<Space>*)<Space>malloc<Space>(<++>*sizeof(int));<Esc>/}<Enter>Ofree(Tab);<Esc>?{<Enter>/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,dm <Esc>Oint<Space>**Tab;<Enter>Tab=<Space>(int<Space>**)<Space>malloc<Space>(nblignes*sizeof(int*));<Enter>for<Space>(int<Space>i=0;i<nblignes;i++)<Enter>{<Enter>Tab[i]=(int<Space>*)<Space>malloc<Space>(nbcol*sizeof(int));<Enter>}<Esc>/}<Enter>Ofree(Tab);<Esc>?{<Enter>?{<Enter>Oint<Space>nblignes=<++>;<Enter>int<Space>nbcol=<++>;<Esc>2k/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,fl float<Space><++><Space>=0.0<Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,int int<Space><++><Space>=0<Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,inc <Esc>?#include<Enter>o#include<<++>.h><Esc>/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,fun int<Space><++>(int i1)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,po int<Space>*<++><Space>=&<++><Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,pr printf("<++>=%d\n",<++>);<Esc>0/<++><Enter><Esc>c4l
autocmd FileType c inoremap  ,co <Esc>A//<++>;<Esc>0/<++><Enter><Esc>c4l
autocmd BufNewFile *.c :r !cat $HOME/.config/Scripts/cbang
augroup END
" }}}
" C++ Shortcuts---------------------- {{{
augroup C++
"removing cpp completion for ale
"autocmd FileType cpp let g:ale_completion_enabled = 0
"add a marker
autocmd FileType cpp nnoremap <leader>m <Esc>o//<++>---------------------- {{{<Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap <leader>m <Esc>o//<++>---------------------- {{{<Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp nnoremap <leader>ù <Esc>o//}}}<Esc>
autocmd FileType cpp inoremap <leader>ù <Esc>o//}}}<Enter>

autocmd FileType cpp map F10 <Esc>:w!<Enter>:!make clean && make \|\| g++ -g -Wall % -o main <Enter> <Enter>:terminal<Enter>./main

autocmd FileType cpp inoreabbrev mai int<Space>main(int argc, char ** argv)<Enter><Space>{<Enter><++><Enter>return 0;<Enter>}<Esc>3k/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev prd std::cout << "<++>vaut "<< <++> << std::endl;<Esc><Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev template template<typename<template>><Enter><typeFonction> <nomFonction>(<template>& t1<Enter>{<Enter><++><Enter>}<Esc>4k0/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev try try{<Enter><++><Enter>}<Enter>catch(const std::string& str)<Enter>{<Enter>std::cerr << str << std::endl;<Enter>]}<Esc>6k0/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev pr std::cout << "<++>" << std::endl;<Esc>0:call CompleteMe()<Enter>

autocmd FileType cpp inoreabbrev list std::list<<TypeListe>> <Liste>;<Esc>:call CompleteMe("<TypeListe>","<Liste>")<Enter>


autocmd FileType cpp inoreabbrev  fo for<Space>(int<Space>i=0;i<<++>;i++)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++>;<Enter><Esc>c4l
autocmd FileType cpp inoreabbrev  iff if<Space>(<++>)<Enter><Space>{<Enter><++><Enter>}<Esc>4k:call CompleteMe()<Enter>
"Input and out streams
autocmd FileType cpp inoreabbrev  ofstream std::ofstream ofs("<++>");0/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev  ifstream std::ifstream ifs("<++>");0/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev  wstr ofs << "<++>" <<std::endl;0/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev  rstr ifs >> <++>;0/<++><Enter><Esc>c4l

autocmd FileType cpp inoreabbrev  oss std::ostringstream <stringname>;<Enter>std::string s = <stringname>;<Esc>:call CompleteMe("<stringname>")<Enter>

autocmd FileType cpp inoreabbrev  sm int<Space>mat<++>=[N,N];<Enter>for(int<Space>i=0;i<N;i++)<Space><Enter>{<Enter>for(int<Space>j=0;j<N;j++)<Enter><Space>{<Enter>mat<++>[i][j]=rand()%100;<Enter>}<Enter>}<Esc>8k/<++><Enter><Esc>c4l
autocmd FileType cpp inoreabbrev  tab int<Space>tab<++>[N];<Enter>for(int<Space>i=0;i<N;i++)<Enter><Space>{<Enter>tab[i]=rand()%100;<Enter>}<Esc>5k/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>dt <Esc>Oint<Space>*Tab;<Enter>Tab=<Space>(int<Space>*)<Space>malloc<Space>(<++>*sizeof(int));<Esc>/}<Enter>Ofree(Tab);<Esc>?{<Enter>/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>dm <Esc>Oint<Space>**Tab;<Enter>Tab=<Space>(int<Space>**)<Space>malloc<Space>(nblignes*sizeof(int*));<Enter>for<Space>(int<Space>i=0;i<nblignes;i++)<Enter>{<Enter>Tab[i]=(int<Space>*)<Space>malloc<Space>(nbcol*sizeof(int));<Enter>}<Esc>/}<Enter>Ofree(Tab);<Esc>?{<Enter>?{<Enter>Oint<Space>nblignes=<++>;<Enter>int<Space>nbcol=<++>;<Esc>2k/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>fl float<Space><++><Space>=0.0<Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>int int<Space><++><Space>=0<Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>inc <Esc>?#include<Enter>o#include<<++>><Esc>/<++><Enter><Esc>c4l

autocmd FileType cpp inoremap  {{ <Enter>{<Enter><++><Enter>}<Esc>?<++><Enter>c4l

autocmd FileType cpp inoreabbrev func int<Space><++>(int i1)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++><Enter><Esc>c4l
""autocmd FileType cpp inoreabbrev const void<Space>const<Space><++>(int i1)<Enter><Space>{<Enter><++><Enter>}<Esc>4k/<++><Enter><Esc>c4l
""Crée une classe
autocmd FileType cpp inoreabbrev  cl class<Space><nomClasse><Enter>{<Enter>protected:<Enter>int _<var1>;<Enter><Enter>public<space>:<Enter>//Constructeur<Space>de<Space><nomClasse><Enter><nomClasse>()<Enter>{<Enter>_<var1>=0;<Enter>}<Enter>int& <nomClasse>Getter()<Enter>{<Enter>return _<var1>;<Enter>}<Enter>~<nomClasse>()<Enter>{<Enter>}<Enter>};<Esc>6k/<nomClasse><Enter><Esc>:call CompleteMe()<Enter>

autocmd FileType cpp inoreabbrev range for<Space>(const<Space><TypeListe>& i : <Liste>)<Enter><Space>{<Enter><++><Enter>}<Esc>:call CompleteMe("<TypeListe>","<Liste>")<Enter>


autocmd FileType cpp inoremap  <leader>po int<Space>*<++><Space>=&<++><Space>;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>pr std::cout << "<++>=%d\n,<++>" << std::endl ;<Esc>0/<++><Enter><Esc>c4l
autocmd FileType cpp inoremap  <leader>co <Esc>A/* <++> */ Esc>0/<++><Enter><Esc>c4l
autocmd BufNewFile *.cpp :r !cat $HOME/.config/Scripts/cppbang
augroup END
" }}}
" bib Shortcuts---------------------- {{{
augroup bib
""".bib
autocmd FileType bib inoremap ,a @article{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>journal<Space>=<Space>{<++>},<Enter>volume<Space>=<Space>{<++>},<Enter>pages<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
autocmd FileType bib inoremap ,b @book{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>6kA,<Esc>i
autocmd FileType bib inoremap ,c @incollection{<Enter>author<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>booktitle<Space>=<Space>{<++>},<Enter>editor<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
augroup END
" }}}
" Markdown Shortcuts---------------------- {{{
augroup Markdown
autocmd Filetype markdown,rmd map <leader>w yiWi[<esc>Ea](<esc>pa)
autocmd Filetype markdown,rmd inoremap ,n ---<Enter><Enter>
autocmd Filetype markdown,rmd inoremap ,b ****<++><Esc>F*hi
autocmd Filetype markdown,rmd inoremap ,s ~~~~<++><Esc>F~hi
autocmd Filetype markdown,rmd inoremap ,e **<++><Esc>F*i
autocmd Filetype markdown,rmd inoremap ,h ====<Space><++><Esc>F=hi
autocmd Filetype markdown,rmd inoremap ,i ![](<++>)<++><Esc>F[a
autocmd Filetype markdown,rmd inoremap ,a [](<++>)<++><Esc>F[a
autocmd Filetype markdown,rmd inoremap ,1 #<Space><Enter><++><Esc>kA
autocmd Filetype markdown,rmd inoremap ,2 ##<Space><Enter><++><Esc>kA
autocmd Filetype markdown,rmd inoremap ,3 ###<Space><Enter><++><Esc>kA
autocmd Filetype markdown,rmd inoremap ,l --------<Enter>
autocmd Filetype rmd inoremap ,r ```{r}<CR>```<CR><CR><esc>2kO
autocmd Filetype rmd inoremap ,p ```{python}<CR>```<CR><CR><esc>2kO
autocmd Filetype rmd inoremap ,c ```<cr>```<cr><cr><esc>2kO
augroup END
" }}}
" xml Shortcuts---------------------- {{{
augroup xml
""".xml
autocmd FileType xml inoremap ,e <item><Enter><title><++></title><Enter><guid<space>isPermaLink="false"><++></guid><Enter><pubDate><Esc>:put<Space>=strftime('%a, %d %b %Y %H:%M:%S %z')<Enter>kJA</pubDate><Enter><link><++></link><Enter><description><![CDATA[<++>]]></description><Enter></item><Esc>?<title><enter>cit
autocmd FileType xml inoremap ,a <a href="<++>"><++></a><++><Esc>F"ci"
" }}}
" YouCompleteMe?---------------------- {{{
augroup ycm
let g:ycm_confirm_extra_conf = 0
"""C language (resp C++)
autocmd Filetype c let g:ycm_global_ycm_extra_conf = '$XDG_CONFIG_HOME/nvim/ycm_extra_conf_c.py'
autocmd Filetype c++ let g:ycm_global_ycm_extra_conf = '$XDG_CONFIG_HOME/nvim/ycm_extra_conf_c++.py'
augroup END
" }}}
" Numbers remaps, french keymap---------------------- {{{
augroup numbers
nnoremap & 1
nnoremap 1 &
nnoremap é 2
nnoremap " 3
nnoremap ' 4
nnoremap ( 5
nnoremap - 6
nnoremap è 7
nnoremap _ 8
nnoremap ç 9
nnoremap à 0
vnoremap & 1
vnoremap 1 &
vnoremap é 2
vnoremap " 3
vnoremap ' 4
vnoremap ( 5
vnoremap - 6
vnoremap è 7
vnoremap _ 8
vnoremap ç 9
vnoremap à 0
onoremap & 1
onoremap 1 &
onoremap é 2
onoremap " 3
onoremap ' 4
onoremap ( 5
onoremap - 6
onoremap è 7
onoremap _ 8
onoremap ç 9
onoremap à 0
augroup END
" }}}
" Some functions---------------------- {{{

"Function to increase numbers
" Add argument (can be negative, default 1) to global variable i.
" Return value of i before the change.
function Inc(...)
	let result = g:i
	let g:i += a:0 > 0 ? a:1 : 1
	return result
endfunction

function CompleteMe(...)
	let WordList=['<nomClasse>','<var1>']
	for i in range(1,a:0)
		call add(WordList,a:{i})
	endfor
	for index in WordList
		if search(index)
			if index=~'<nomClasse>'
				let entree=input('Quel est le nom de la classe? ')
				let l = 1
				for line in getline(1,'$')
					call setline(l, substitute(line, index,entree, 'g'))
					let l = l + 1
				endfor
			else
				let entree=input('Quel est le nom de la variable '.index.' ')
				let l = 1
				for line in getline(1,'$')
					call setline(l, substitute(line,index,entree, 'g'))
					let l = l + 1
				endfor
			endif
		endif
	endfor
	if search('<++>')
		execute "normal 0/<++>\<Enter>\<Esc>d4l"
		startinsert
	endif
endfunction
" }}}
