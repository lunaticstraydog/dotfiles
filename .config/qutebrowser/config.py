# -*- mode: python -*-
import os
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
from enum import Enum
config_version: 1
config.load_autoconfig(False)
config = config  # type: ConfigAPI # noqa: F821
c = c  # type: ConfigContainer # noqa: F821
initial_start = c.tabs.background == False

if True:
    # ui

    #config.set("colors.webpage.darkmode.enabled",True)
    c.completion.scrollbar.width = 10
    c.tabs.position = 'top'
    #c.content.user_stylesheets="~/temp/solarized-everything-css/css/solarized-all-sites-dark.css"
    c.tabs.show = 'multiple'
    c.tabs.favicons.show = "never"
    c.content.cookies.store=True
    c.tabs.show_switching_delay=100
    config.bind('C', 'spawn --userscript qute-pass --dmenu-invocation dmenu')
#    config.bind('C', 'spawn --userscript qute-pass --dmenu-invocation dmenu --password-only')
    c.content.javascript.enabled=True
    c.content.cookies.accept="no-3rdparty"
#   c.tabs.width.indicator = 0
    config.set('content.cookies.accept',"no-3rdparty", '*://www.qwant.com/*')
    config.set('content.cookies.accept',"no-3rdparty", '*://arstechnica.com/*')
    config.set('content.cookies.accept',"no-3rdparty", '*://www.nextinpact.com/*')
    config.set('content.cookies.accept',"no-3rdparty", '*://compte.nextinpact.com/*')
    config.set('content.cookies.accept',"no-3rdparty", '*://www.inpact-hardware.com/*')
    config.set('content.cookies.accept',"no-3rdparty",'*://www.comptoir-hardware.com/*')
    config.set('content.cookies.accept',"no-3rdparty",'*://searx.sunless.cloud/*')
    config.set('content.cookies.accept',"no-3rdparty",'*://theconversation.com/*')
    config.set('content.cookies.accept',"no-3rdparty",'*://duckduckgo.com/*')
    config.set('content.cookies.accept',"no-3rdparty",'*://deepl.com/*')
    config.set('content.javascript.enabled', True, '*://theconversation.com/*')
    config.set('content.javascript.enabled', True, '*://searx.sunless.cloud/*')
    config.set('content.javascript.enabled', True, '*://www.nextinpact.com/*')
    config.set('content.javascript.enabled', True, '*://github.com/*')
    config.set('content.javascript.enabled', True, '*://arstechnica.com/*')
    config.set('content.javascript.enabled', True, '*://www.inpact-hardware.com/*')
    config.set('content.javascript.enabled', True, '*://twitter.com/*')
    config.set('content.javascript.enabled', True, '*://www.reddit.com/*')
    config.set('content.javascript.enabled', True, '*://weeklyosm.eu/*')
    config.set('content.javascript.enabled', True, '*://compte.nextinpact.com/*')
    config.set('content.javascript.enabled', True, '*://www.comptoir-hardware.com/*')
    config.set('content.javascript.enabled', True, '*://deepl.com/*')
    config.set('content.javascript.enabled', True, '*://spidr.today/*')
    config.set('content.javascript.enabled', True, '*://scantrad.fr/*')
    config.set('content.javascript.enabled', True, '*://duckduckgo.com/*')
    config.set('content.javascript.enabled', True, '*://monster-no-scantrad.fr/*')
    #c.tabs.title.format = '{title}'
    c.tabs.title.alignment = 'center'
    c.downloads.position = 'bottom'
    c.tabs.favicons.show = "never"

    # behavior
    #c.hints.chars='azertyqsdfghjklmuiop'
    c.downloads.location.prompt = False
    c.hints.scatter = True
    c.url.searchengines = {'DEFAULT': 'https://searx.sunless.cloud/?q={}','wp': 'https://fr.wikipedia.org/?search={}','aw': 'https://wiki.archlinux.org/?search={}', 'os': 'https://wiki.openstreetmap.org/wiki/{}', 'osf': 'https://wiki.openstreetmap.org/wiki/FR:Key:{}'}
    #c.url.searchengines = {'DEFAULT': 'https://www.duckduckgo.com/?q={}&t=web','4': 'https://4chan.org/{}', 'wp': 'https://fr.wikipedia.org/?search={}','aw': 'https://wiki.archlinux.org/?search={}'}
#    c.url.searchengines = {'4': 'https://4chan.org/{}' }
#    c.url.searchengines = {'wp': 'https://fr.wikipedia.org/?search={}' }
#    c.url.searchengines = {'aw': 'https://wiki.archlinux.org/?search={}' }
    c.input.insert_mode.auto_load = True
    c.input.insert_mode.auto_leave = False
    c.tabs.background = True
    c.content.pdfjs = True
    c.editor.command = [ os.environ['EDITOR'] + ' "{}"' ]
    c.auto_save.session = True

    # binds
    config.bind('<Shift-i>', 'config-source')
    config.bind('d', 'tab-close')
    config.bind('x', 'quit --save')
    config.bind('h', 'back')
    config.bind('F','hint all tab')
    config.bind('p','open -p')
    config.bind('K','tab-next')
    config.bind('J','tab-prev')
    config.bind('u','forward')
    config.bind('t','open -t')
    config.bind('<Ctrl-m>','open minimachines.net')
    config.bind('<Ctrl-n>','open nextinpact.com')
    config.bind('<Ctrl-f>','open frandroid.com')
    config.bind('<Ctrl-h>','open comptoir-hardware.com')
    config.bind('<Ctrl-a>','open arstechnica.com')
    config.bind('<Ctrl-z>','open theconversation.com/fr')
    config.bind('W',    'config-cycle content.javascript.enabled'   )
    config.bind('P',    'config-cycle tabs.show switching multiple'   )
    config.bind('a','config-cycle content.user_stylesheets ~/temp/solarized-everything-css/css/solarized-all-sites-dark.css ""')

    config.bind('<Ctrl-Shift-m>','open -t minimachines.net')
    config.bind('<Ctrl-Shift-n>','open -t nextinpact.com')
    config.bind('<Ctrl-Shift-f>','open -t  frandroid.com')
    config.bind('<Ctrl-Shift-p>','open -t lesnumeriques.com')
    config.bind('<Ctrl-Shift-h>','open -t comptoir-hardware.com')
    config.bind('<Ctrl-p>','open lesnumeriques.com')
    config.bind('<Ctrl-Shift-a>','open -t arstechnica.com')
    config.bind('<Ctrl-Shift-z>','open -t theconversation.com/fr')
# mustache templated from current theme
theme = {
    'panel': {
        'height': 25,
    },

    'fonts': {
        'main': 'Roboto Mono Medium',
        'tab_bold': False,
        'tab_size': 12,
    },

    'colors': {
        'bg': {
            'normal': '#E3E3E3',
            'active': '#B9B9B9',
            'inactive': '#F7F7F7',
        },

        'fg': {
            'normal': '#464646',
            'active': '#525252',
            'inactive': '#464646',

            # completion and hints
            'match': '#0A0A0A',
        },
    }
}

# colors
colors = theme['colors']

def setToBG(colortype, target):
    config.set('colors.' + target, colors['bg'][colortype])

def setToFG(colortype, target):
    config.set('colors.' + target, colors['fg'][colortype])

def colorSync(colortype, setting):
    if setting.endswith('.fg'):
        setToFG(colortype, setting)
    elif setting.endswith('.bg'):
        setToBG(colortype, setting)
    elif setting.endswith('.top') or setting.endswith('.bottom'):
        setToFG(colortype, setting)
    else:
        setToFG(colortype, setting + '.fg')
        setToBG(colortype, setting + '.bg')

targets = {
    'normal' : [
        'statusbar.normal',
        'statusbar.command',
        'tabs.even',
        'tabs.odd',
        'hints',
        'downloads.bar.bg',
        ],

    'active' : [
        'tabs.selected.even',
        'tabs.selected.odd',
        'statusbar.insert',
        'downloads.stop',
        'prompts',
        'messages.warning',
        'messages.error',

        'completion.item.selected',

        'statusbar.url.success.http.fg',
        'statusbar.url.success.https.fg',

        'completion.category',
    ],

    'inactive' : [
        'completion.scrollbar',
        'downloads.start',
        'messages.info',
        'completion.fg',
        'completion.odd.bg',
        'completion.even.bg',

        'completion.category.border.top',
        'completion.category.border.bottom',
        'completion.item.selected.border.top',
        'completion.item.selected.border.bottom',
    ],

    'match' : [
        'completion.match.fg',
        'hints.match.fg',
    ]
}

for colortype in targets:
    for target in targets[colortype]:
        colorSync(colortype, target)

setToFG('active', 'statusbar.progress.bg')

config.set('hints.border', '1px solid ' + colors['fg']['normal'])

# tabbar
def makePadding(top, bottom, left, right):
    return { 'top': top, 'bottom': bottom, 'left': left , 'right': right }

# TODO improve this logic
# assume height of font is ~10px, pad top by half match panel height
surround = round((theme['panel']['height'] - 10) / 2)
c.tabs.padding = makePadding(surround,surround,8,8)
c.tabs.indicator.padding = makePadding(0,0,0,0)

# fonts
#c.fonts.monospace = theme['fonts']['main']

tabFont = str(theme['fonts']['tab_size']) + 'pt ' + theme['fonts']['main']
if theme['fonts']['tab_bold']:
    tabFont = 'bold '  + tabFont

#c.fonts.tabs = tabFont
