#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PATH=$PATH:$HOME/.local/bin/:$HOME/.config/Scripts/:
alias ls='ls --color=auto'
alias matlab='env QT_QPA_PLATFORM=xcb /usr/local/MATLAB/R2020a/bin/matlab'
PS1='[\u@\h \W]\$ '
stty -ixon
set -o vi

export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

#if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
	#exec startx
#fi
function cd {
		builtin cd "$@" && ls -F
}

#Basic Aliases
alias dlfile='~/.config/Scripts/dlfile.sh'
alias moon='curl wttr.in/Moon'
alias v="vim"
alias clean="yay -Sc"
alias sv="sudo vim"
alias munster="bash ~/temp/Munsterac/Munster-Academy-all/Munster\ Academy.sh"
alias ls='ls -l --color=auto --group-directories-first'
alias disk="du --max-depth=1 -x -k | sort -n | awk 'function human(x) { s="KMGTEPYZ"; while (x>=1000 && length(s)>1) {x/=1024; s=substr(s,2)} return int(x+0.5) substr(s,1,1)"iB" } {gsub(/^[0-9]+/, human($1)); print}'"
alias lsa='ls -A'
alias b="cd .. && ls -a"
alias q="exit"
alias e="exit"
alias cavestory="bash /home/lunaticstraydog/travaux/doukutsu/NXEngine-v2.6.3-Linux/nx -F -p 15 -n 4"
alias pi="yay -S"
alias pss="yay -Ss"
alias prns="yay -Rns"
alias psyu="yay -Syu "
alias psa="yay -Syu"
alias PSyyu="sudo pacman -Syyu"
alias PQn="sudo pacman -Qn"
alias pr="yay -R"

alias cp="echo Pour utiliser l\'option reflink, utilisez cpr && cp"
alias cpr="echo Utilisation de l\'option reflink && cp --reflink"

#Term only
alias mute="pamixer -m"
alias vd="pamixer -d 10"
alias vu="pamixer -i 10"
alias p="mocp -G &> /dev/null"
alias next="mocp -f &> /dev/null"
alias prev="mocp -r &> /dev/null"
#alias mnt="sudo bash ~/.config/Scripts/mount.sh"
alias umnt="sudo bash ~/.config/Scripts/unmount.sh"
alias newnet="sudo systemctl restart NetworkManager"

#etc
alias screenfetch="screenfetch -t"
alias ytdl="youtube-dl -ic"
alias yta="youtube-dl -xic"
alias ein="ssh -l einchan -p 22 104.238.215.7"
alias starwars="telnet towel.blinkenlights.nl"
alias youtube="youtube-viewer"
alias YT="youtube-viewer"
alias syt="youtube-viewer"
alias Txa="cp ~/Documents/LaTeX/article.tex"
alias Txs="cp ~/Documents/LaTeX/beamer.tex"
alias Txh="cp ~/Documents/LaTeX/handout.tex"

givegit() { git clone http://github.com/$1.git ;}
weath() { curl wttr.in/$1 ;}
alias "lsl"='ls -lh --color=auto'
alias yt='mpv "--ytdl-format=bestvideo[height<=?720]+bestaudio/best"'
#PS1='[\u@\h \W]\$ '
#alsi
export PROMPT_COMMAND='history -a'; history -r
shopt -s autocd
export EDITOR=nvim
export VISUAL=nvim
export TERM=termite
export term=termite
HISTSIZE=50000000
HISTFILESIZE=10000000000
shopt -s histappend
#export GAME_DEBUGGER="hl2debug"
#alias cat='lolcat'
alias scat='catsay MEOW && cat'
alias wsteam="wine ~/.wine/drive_c/'Program Files (x86)'/Steam/Steam.exe  -no-cef-sandbox"
alias vpn="bash ~/.config/vpn"
alias sudo='sudo '
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
export browser=firefox
#[[ -f /usr/share/nwd/preexec.sh ]] && . /usr/share/nwd/prexec.sh
alias h="cd ~ && ls -a"
alias cf="cd ~/.config && ls -a"
alias D="cd ~/Documents && ls -a"
alias l="cd ~/Documents/LaTeX && ls -a"
alias d="cd ~/Downloads && ls -a"
alias pp="cd ~/Pictures && ls -a"
alias pw="cd ~/Pictures/Wallpapers && ls -a"
alias vv="cd ~/Videos && ls -a"
alias c="cd ~/.config && ls -a"
alias m="cd ~/Music && ls -a"
alias cfb="vim ~/.bashrc"
alias cfz="vim ~/.zshrc"
alias cfv="vim ~/.config/nvim/init.vim"
alias cfr="vim ~/.config/ranger/rc.conf"
alias cfs="vim ~/.config/sway/config"
alias cfq="vim ~/.config/qutebrowser/config.py"
alias cfm="vim ~/.config/mutt/muttrc"
alias cfM="vim ~/.moc/keymap"
alias cff="vim ~/.config/Scripts/folders"
alias cfc="vim ~/.config/Scripts/configs"
alias cft="vim ~/.config/termite/config"
alias cfT="vim ~/.tmux.conf"
alias eb="vim ~/Documents/LaTeX/uni.bib"
alias cfg="vim ~/.config/mutt/gmail.conf"
alias cfC="vim ~/.config/mutt/cock.conf"
alias cfa="vim ~/.config/mutt/aliases"
alias zoom='env QT_QPA_PLATFORM=xcb zoom'
alias ripcord='env QT_QPA_PLATFORM=xcb ripcord'
#alias zoom='~/travaux/swallow env QT_QPA_PLATFORM=xcb zoom'
alias josm='~/travaux/swallow josm'
alias qutebrowser='~/travaux/swallow qutebrowser'
#alias firefox='~/travaux/swallow firefox'
#alias mpv='~/travaux/swallow mpv'
alias discord='~/travaux/swallow discord'
alias skype='~/travaux/swallow env skype'
alias doxywizard='~/travaux/swallow doxywizard'
alias zathura='~/travaux/swallow zathura'
alias libreoffice='~/travaux/swallow libreoffice'
alias mupdf='~/travaux/swallow mupdf'
alias swallow='~/travaux/swallow'
alias sw='~/travaux/swallow'
alias sudo='sudo -v; sudo '
export XDG_CONFIG_HOME="$HOME/.config"
#alias zoom='~/travaux/swallow env QT_QPA_PLATFORM=xcb zoom'
# If running from tty1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
		rm ~/.Xauthority
		export _JAVA_AWT_WM_NONREPARENTING=1
		alias freecad='env QT_QPA_PLATFORM=xcb freecad'
		alias onlyoff='env QT_QPA_PLATFORM=xcb onlyoffice-desktopeditors'
		#export QT_QPA_PLATFORM=wayland-egl;xcb
		#export QT_QPA_PLATFORM=xcb
		#export QT_STYLE_OVERRIDE=gtk2
		export XDG_CURRENT_DESKTOP=sway
		export XDG_SESSION_TYPE=wayland
		export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
		export MOZ_ENABLE_WAYLAND=1
		export QT_QPA_PLATFORMTHEME=qt5ct
		exec sway
fi
export GIT_SSL_NO_VERIFY=true

mkcd() {
	local dir="$1"
	local dir="${dir:=$HOME}"
	if [[ -d "$dir" ]]; then
		echo "bash: mkcd: $dir: Directory Exists"
	else
		mkdir "$dir";cd "$dir" >/dev/null
	fi
}
eval "$(thefuck --alias)"
alias config='/usr/bin/git --git-dir=/home/lsdog/.cfg/ --work-tree=/home/lsdog'
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
bind -x '"\C-p": vim $(fzf);'
